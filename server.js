require('dotenv').config()
const responseAPI = require('./helpers/responseAPI.helper')
const authMiddleware = require('./middlewares/auth.middleware')
const bodyParser = require('body-parser')
const express = require('express')
const session = require('express-session')
const MongoStore = require('connect-mongo');
const authController = require('./controllers/auth.controller')
const validate = require('./validations/userRegister.validate')
const app = new  express()


const mongooes = require('mongoose')
mongooes.connect(process.env.MONGO_URL).then(() => {
    console.log("connected to database")
}).catch(err => {
    console.log("Not connected to database error!", err)
})

app.use(session({
    secret: 'mySecretKey',
    resave: true,
    saveUninitialized: false,
    store: MongoStore.create({ mongoUrl: 'mongodb://localhost:27017/sso' })
}));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/api/login', authMiddleware.isAuth, authController.login)
app.post('/api/register', validate.result, authController.register)
app.use(responseAPI.listenError);

const server = require('http').Server(app)
server.listen(process.env.PORT, () => console.log(`Server is running: http://localhost:${process.env.PORT}`))