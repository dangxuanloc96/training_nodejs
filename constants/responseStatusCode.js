module.exports = {
    success: 200,
    page_not_found: 404,
    internal_server_error: 500,
    unauthorized: 401,
}