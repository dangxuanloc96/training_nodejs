exports.isAuth = (req, res, next) => {
    if(req.session.isAuth) {
        next();
    }
    throw new Error("Chưa xác thực");
}