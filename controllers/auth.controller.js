const responseAPI = require('../helpers/responseAPI.helper')
const authService = require('../services/auth.service')
const statusServerCode = require('../constants/responseStatusCode')
const responseMessage = require('../constants/responseMessage')

exports.login = async (req, res, next) => {
    try {
        req.session.isAuth = true
        return res.json({});
        const {name, password} = req.body;

        if (name == null || name.length == "") throw new Error('Tên không được để trống')
        if (password == null || name.length == "") throw new Error('Mật khẩu không được để trống')

        const result = await authService.login(name, password)

        res.json(responseAPI.resultAPI(statusServerCode.success, responseMessage.login_success, result))
    } catch (err) {
        next(err);
    }
}

exports.register = async (req, res, next) => {
    try {
        const {name, email, password, confirm_password} = req.body;
        res.json({})
    } catch (err) {
        next(err)
    }
}