const logger = require('../logs/winston');

var resultAPI = (code, message, result) => {
    let response = {
        code: code,
        message: message,
    };
    if (result !== null) {
        response.data = result;
    }

    return response;
};

var listenError = (err, req, res, next) => {
    res.status(err.status || 500);
    logger.error(err);
    res.json({
        code: err.status || 500,
        message: err.message,
        data: null
    });
};

module.exports = {
    resultAPI: resultAPI,
    listenError: listenError,
}