const {body, validationResult} = require("express-validator")
const message = require('../constants/messageValidate')
const response = require('../helpers/responseAPI.helper')

const result = (req, res, next) => {
    body('name').trim().not().isEmpty().withMessage("name" + message.is_empty)
    body('email').trim().not().isEmpty().withMessage("email" + message.is_empty)
    body('password').trim().not().isEmpty().withMessage("password" + message.is_empty)
    body('confirm-password').trim().not().isEmpty().withMessage("confirm-password" + message.is_empty)

    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return response.listenError(errors)
    }
    next()

};

module.exports = {
    result: result
};